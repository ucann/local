# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'json'
require 'yaml'

VAGRANTFILE_API_VERSION ||= "2"
confDir = $confDir ||= File.expand_path(File.dirname(__FILE__))

ucannYamlPath = confDir + "/build/local/config/ucann.yaml"
ucannJsonPath = confDir + "/build/local/config/ucann.json"

require File.expand_path(File.dirname(__FILE__) + '/build/local/ucann.rb')

Vagrant.require_version '>= 1.9.0'

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

    if File.exist? ucannYamlPath then
        settings = YAML::load(File.read(ucannYamlPath))
    elsif File.exist? ucannJsonPath then
        settings = JSON.parse(File.read(ucannJsonPath))
    else
        abort "Ucann settings file not found in #{confDir}"
    end

    Ucann.configure(config, settings)

    if defined? VagrantPlugins::HostsUpdater
        config.hostsupdater.aliases = settings['sites'].map { |site| site['map'] }
    end

end
