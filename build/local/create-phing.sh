#!/usr/bin/env bash

P=$1

cd `echo $P | rev | cut -d '/' -f 4- | rev`

/usr/local/bin/phing -f $P
