#!/usr/bin/env bash

U="ubuntu"

if [[ ! `dpkg --get-selections $1` ]]; then
apt-get install -y $1
fi

if [[ ! -f /usr/bin/git ]]; then
ufw disable
/usr/local/bin/composer self-update

if [[ ! -d  /home/$U/run/ ]]; then
mkdir /home/$U/run/
fi

if [[ ! -d /home/$U/log/ ]]; then
mkdir /home/$U/log/
fi

if [[ ! -d /home/$U/tmp/ ]]; then
mkdir /home/$U/tmp/
fi

fi

echo "StrictHostKeyChecking no" > /home/ubuntu/.ssh/config
echo "UserKnownHostsFile=/dev/null" >> /home/ubuntu/.ssh/config
chmod 0600 /home/ubuntu/.ssh/config
chown -R $U:$U /home/$U/
