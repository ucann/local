#!/usr/bin/env bash

#/etc/apt/sources.list.d/ondrej-ubuntu-php-xenial.list
#deb http://ppa.launchpad.net/ondrej/php/ubuntu xenial main

type="nts";
ver="7.1.7";

if [[ ! -f /usr/bin/php ]]; then

if [[ $type == "nts" ]]; then
add-apt-repository -y ppa:ondrej/php
apt-get update
apt-get install -y pkg-config libssl-dev libxml2 libxml2-dev libcurl4-gnutls-dev libenchant-dev libpq-dev libjpeg-turbo8-dev libpng-dev libfreetype6-dev libpspell-dev libxslt-dev
echo "Install PHP7.2.........."
apt-get install -y php7.2-fpm php7.2-bcmath php7.2-soap php7.2-cli php7.2 php7.2-memcached php7.2-curl php7.2-gmp php7.2-xml php7.2-mbstring php7.2-dev php7.2-igbinary php7.2-imap php7.2-pgsql php7.2-zip php7.2-intl php7.2-gd php7.2-iconv php7.2-redis && update-rc.d php7.2-fpm defaults

rm -rf /etc/php/7.2
cp -rf /home/ubuntu/htdocs/build/local/config/php/ /etc/

fi

fi

rm -rf /etc/php/7.2

cp -rf /home/ubuntu/htdocs/build/local/config/php/ /etc/
mkdir -p /etc/php/7.2/fpm/conf.d || true
mkdir -p /etc/php/7.2/cli/conf.d || true

cp -rf /home/ubuntu/htdocs/build/local/config/php/7.2/mods-available/* /etc/php/7.2/fpm/conf.d/ || true
cp -rf /home/ubuntu/htdocs/build/local/config/php/7.2/mods-available/* /etc/php/7.2/cli/conf.d/ || true


if [[ ! -f /usr/local/bin/composer ]]; then
cd /tmp
curl -s https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
fi

if [[ ! -f /usr/local/bin/phing ]]; then
cd /tmp
curl -sSL http://www.phing.info/get/phing-latest.phar > /usr/local/bin/phing
chmod +x /usr/local/bin/phing
fi
