#!/usr/bin/env bash

if [[ ! -f /usr/bin/clickhouse-client ]]; then
echo "deb http://repo.yandex.ru/clickhouse/xenial stable main" > /etc/apt/sources.list.d/clh.list
apt-key adv --keyserver keyserver.ubuntu.com --recv E0C56BD4
apt-get update
apt-get install clickhouse-client clickhouse-server-common -y
rm -rf /etc/clickhouse-client/
rm -rf /etc/clickhouse-server/
fi
cp -rf /home/ubuntu/htdocs/build/local/config/clickhouse-client /etc/
cp -rf /home/ubuntu/htdocs/build/local/config/clickhouse-server /etc/
systemctl enable clickhouse-server &>> /dev/null
service clickhouse-server restart


