#!/bin/bash

IP="10.1.1.100"
NAME="dev"

if [[ "$OSTYPE" == "linux-gnu" ]]; then
echo "LINUX"
cp -f docker-compose.yml_docker-machine docker-compose.yml
DIR=`pwd | sed 's/home/hosthome/'`
docker-machine create  --driver virtualbox --virtualbox-hostonly-cidr 10.1.1.10/24 $NAME
UN=`docker-machine ssh $NAME uname -s`
UNM=`docker-machine ssh $NAME uname -m`

if [[ `docker-machine ssh $NAME find /usr/local/bin/docker-compose 2>&1 > /dev/null` ]]; then
	#echo "ifconfig eth1 $IP netmask 255.255.255.0 broadcast 10.1.1.255 up" | docker-machine ssh $NAME sudo tee /var/lib/boot2docker/bootsync.sh > /dev/null
	#docker-machine restart $NAME
	#docker-machine regenerate-certs -f $NAME
	docker-machine ssh $NAME sudo curl -L https://github.com/docker/compose/releases/download/1.19.0/docker-compose-$UN-$UNM -o /usr/local/bin/docker-compose
	docker-machine ssh $NAME sudo chmod +x /usr/local/bin/docker-compose
fi

docker-machine ssh $NAME '
cd '$DIR';
docker-compose down;
docker-compose pull;
docker-compose build --no-cache;
docker-compose up;
echo "OK";'

fi


if [[ "$OSTYPE" == "darwin"* ]]; then
echo "OSX"
cp -f docker-compose.yml_docker-machine docker-compose.yml
#export VIRTUALBOX_HOSTONLY_CIDR=$IP/32
DIR=`pwd`
docker-machine create  --driver virtualbox --virtualbox-hostonly-cidr 10.1.1.10/24 $NAME
UN=`docker-machine ssh $NAME uname -s`
UNM=`docker-machine ssh $NAME uname -m`

if [[ `docker-machine ssh $NAME find /usr/local/bin/docker-compose 2>&1 > /dev/null` ]]; then
	#echo "ifconfig eth1 $IP netmask 255.255.255.0 broadcast 10.1.1.255 up" | docker-machine ssh $NAME sudo tee /var/lib/boot2docker/bootsync.sh > /dev/null
	#docker-machine restart $NAME
	#docker-machine regenerate-certs -f $NAME
	docker-machine ssh $NAME sudo curl -L https://github.com/docker/compose/releases/download/1.19.0/docker-compose-$UN-$UNM -o /usr/local/bin/docker-compose
	docker-machine ssh $NAME sudo chmod +x /usr/local/bin/docker-compose
fi

docker-machine ssh $NAME '
cd '$DIR';
docker-compose down;
docker-compose pull;
docker-compose build --no-cache;
docker-compose up;
echo "OK";'
fi


